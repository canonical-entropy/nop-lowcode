# Nop Platform 2.0

#### 介绍

Nop Platform 2.0是基于可逆计算原理从零开始构建的新一代低代码平台，它致力于克服低代码平台无法摆脱穷举法的困境，从理论层面超越组件技术，有效的解决粗粒度软件复用的问题。

nop-entropy是Nop平台的后端部分。它采用Java语言实现，不依赖第三方框架，可以和Quarkus或者Spring框架集成在一起使用。nop-entropy支持GraalVM技术，可以借助于Quarkus或者SpringNative框架编译为原生镜像。nop-entropy的设计目标是提供了简单易用的领域语言工作台，通过增加简单的元数据定义，就可以自动得到对应的解析器、验证器、IDE插件、调试工具等，并自动为DSL领域语言增加模块分解、差量定制、元编程等通用语言特性。在这一点上，它类似于Jetbrains公司的[MPS产品](https://www.jetbrains.com/mps/)，只是它的设计原理和技术实现路径与MPS有着本质性差别。

目前nop-entropy主要包含XLang语言的实现，以及ORM/IoC/Config/GraphQLEngine/ReportEngine/JobEngine等基础框架，后续规划包括RuleEngine/BatchEngine/WorkflowEngine/BI等业务开发常用部分。nop-entropy采用云原生设计，可以单机运行，也可以作为分布式集群运行，可以提供在线的API服务，也可以将针对单个业务对象的在线服务自动包装为针对批处理文件的批处理任务，内置分布式事务和多租户支持。对于大多数业务应用场景均提供相应的模型支持，只需少量配置即可完成主要功能，大大降低对手工编码的需求。nop-entropy在开发期可以作为支持增量式开发的低代码平台，自动生成各类代码以及相关文档，在运行期可以作为面向最终用户的无代码平台的支撑技术，允许客户在线调整业务模块功能，以所见即所得的方式进行产品迭代。

nop-chaos是Nop平台的前端部分，基于Vue3.0、ant-design-vue、百度AMIS、logicflow、xspreadsheet等技术实现。它与nop-chaos相结合，可以形成类似[百度爱速搭](https://aisuda.baidu.com/)的完整解决方案。nop-chaos是一个通用的前端低代码运行容器，它打包之后可以直接嵌入到后端的Java服务中。**在一般的业务开发中，只需要在Java端增加JSON文件和一些简单的js脚本库即可，并不需要重新编译打包nop-chaos项目**。

nop-chaos目前主要使用[百度AMIS框架](https://github.com/baidu/amis)来渲染后端返回的低代码页面，但是它的设计本身是通用的，大量操作都是在与框架无关的JSON结构层面完成，可以很容易的适配其他低代码引擎。后期考虑会增加对[阿里LowCodeEngine](https://github.com/alibaba/lowcode-engine)低代码引擎的适配。

#### 设计原理

[可逆计算：下一代软件构造理论](https://zhuanlan.zhihu.com/p/64004026)

[可逆计算的技术实现](https://zhuanlan.zhihu.com/p/163852896)

[从张量积看低代码平台的设计](https://zhuanlan.zhihu.com/p/531474176)

[低代码平台需要什么样的ORM引擎？](https://zhuanlan.zhihu.com/p/543252423)

[为什么百度AMIS是一个优秀的设计](https://zhuanlan.zhihu.com/p/599773955)